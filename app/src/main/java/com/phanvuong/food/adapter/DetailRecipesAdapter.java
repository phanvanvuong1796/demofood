package com.phanvuong.food.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.phanvuong.food.R;

/**
 * Created by bongm on 2017-07-13.
 */

public class DetailRecipesAdapter extends RecyclerView.Adapter<DetailRecipesAdapter.DetailRecipesViewHolder>{


    @Override
    public DetailRecipesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipes_list_details, parent, false);
        return new DetailRecipesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DetailRecipesViewHolder holder, int position) {
        holder.imgDetailRecipes.setBackgroundResource(R.mipmap.recipe1);
    }

    @Override
    public int getItemCount() {
        return 6;
    }

    public class DetailRecipesViewHolder extends RecyclerView.ViewHolder{
        ImageView imgDetailRecipes;
        public DetailRecipesViewHolder(View itemView) {
            super(itemView);
            imgDetailRecipes = itemView.findViewById(R.id.img_recipes_details);
        }
    }
}
