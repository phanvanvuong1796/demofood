package com.phanvuong.food.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.phanvuong.food.R;

import java.util.Random;

/**
 * Created by bongm on 2017-07-13.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>{

    public interface ICallBack{
        void onItemClick(int position);
    }

    private ICallBack mICallBack;

    public void setmICallBack(ICallBack mICallBack) {
        this.mICallBack = mICallBack;
    }

    int[] imgIcon = new int[]{R.mipmap.cat1, R.mipmap.cat2, R.mipmap.cat3, R.mipmap.cat4, R.mipmap.cat5, R.mipmap.cat6};
    String[] cateName = new String[]{"Appetizer", "Bevarages", "Breakfasts", "Desserts", "Main Dishes", "Side Dishes"};
    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        final int pos = position;
        Random random = new Random();
        int i = random.nextInt(imgIcon.length - 1);
        holder.imgIconCate.setBackgroundResource(imgIcon[i]);
        holder.txtNameCate.setText(cateName[i]);
        holder.txtQuantity.setText(""+random.nextInt(100));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mICallBack.onItemClick(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return 15;
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder{
        ImageView imgIconCate;
        TextView txtNameCate;
        TextView txtQuantity;
        View itemView;
        public CategoryViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            imgIconCate = itemView.findViewById(R.id.img_icon_cate);
            txtNameCate = itemView.findViewById(R.id.txt_cate_name);
            txtQuantity = itemView.findViewById(R.id.txt_cate_quantity);
        }
    }
}
