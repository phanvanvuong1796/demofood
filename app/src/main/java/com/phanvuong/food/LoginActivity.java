package com.phanvuong.food;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private ImageButton btnLoginTw;
    private ImageButton btnLoginFb;
    private ImageButton btnLogin;
    private EditText edtUserName;
    private EditText edtPassWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLoginTw = (ImageButton) findViewById(R.id.btn_login_tw);
        btnLoginFb = (ImageButton) findViewById(R.id.btn_login_fb);
        btnLogin = (ImageButton) findViewById(R.id.btn_login);
        edtUserName = (EditText) findViewById(R.id.edt_username);
        edtPassWord = (EditText) findViewById(R.id.edt_password);

        btnLogin.setOnClickListener(this);
        btnLoginTw.setOnClickListener(this);
        btnLoginFb.setOnClickListener(this);
    }

    private void startMainActivity(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btn_login_tw:
                startMainActivity();
                break;
            case R.id.btn_login_fb:
                startMainActivity();
                break;
            case R.id.btn_login:
                startMainActivity();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
