package com.phanvuong.food.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.phanvuong.food.R;

public class CategoryFragment extends Fragment {


    private TabLayout mTabLayout;
    private ViewPager mViewPager;



    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab, container, false);
        //View viewParent = (View) container.getParent();
//        mAppBarLayout = viewParent.findViewById(R.id.appbar);
//        mTabLayout = new TabLayout(getActivity());
//        mTabLayout.setBackgroundColor(Color.parseColor("#5B5252"));
//        mTabLayout.setTabTextColors(Color.parseColor("#919090"), Color.parseColor("#FFFFFF"));
        mTabLayout = view.findViewById(R.id.tabs);
        mTabLayout.setTabTextColors(Color.parseColor("#A0A0A0"), Color.parseColor("#FFFFFF"));
        //mAppBarLayout.addView(mTabLayout);

        mViewPager = view.findViewById(R.id.viewpager);
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getFragmentManager());
        mViewPager.setAdapter(pagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter{

        String[] str = new String[]{"Recipes", "Wine", "Profile"};

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0: return new ListCategoryFragment();
                case 1: return new ListCategoryFragment();
                case 2: return new ListCategoryFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return str[position];
        }


    }



}
